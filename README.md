See the following link for needed credentials:
https://dev.twitter.com/ads/tutorials/getting-started

Application keys:
Keep the "Consumer Secret" a secret. This key should never be human-readable in your application.

Access tokens:
These can be used to make API requests on your own account's behalf. Do not share your access token secret with anyone.


See the following when planning to extend the API with more functionality:
https://dev.twitter.com/ads/reference

Useful links:
https://dev.twitter.com/ads/analytics/metrics-and-segmentation

https://dev.twitter.com/ads/overview/obtaining-ads-account-access

https://dev.twitter.com/oauth/overview/single-user


MAJOR TODO: UPDATE FORK
TwitterOAuth.php / http() / .json should be removed from sprintf