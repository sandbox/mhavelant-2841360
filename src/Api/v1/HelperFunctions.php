<?php

namespace Drupal\twitter_ads_api\Api\v1;

/**
 * Class Includes.
 *
 * @package Drupal\twitter_ads_api\Api\v1
 */
class HelperFunctions {

  /**
   * Helper function.
   *
   * Generates a comma-separated list of strings from the $array.
   * Each string consists of $batchLimit number of $property items.
   *
   * @param \stdClass[] $array
   *   An array of stdClass object.
   * @param string $groupBy
   *   A property of the stdClass objects. We aggregate by this.
   * @param string $stringifyBy
   *   A property of the stdClass objects. These are the aggregated values.
   * @param string $separator
   *   The $property separator character.
   * @param int $batchLimit
   *   An aggregation limit.
   *
   * @return array
   *   An array of strings grouped by $groupBy.
   *   The strings are lists of $separator separated $property items.
   *   values. (E.g. ['one,two,three', 'four,five,six'])
   */
  public static function stringifyAndGroupArray(array $array, $groupBy = 'account_id', $stringifyBy = 'id', $separator = ',', $batchLimit = 20) {
    $aggregator = [];

    $arraySize = count($array);
    $arrayIndex = 0;

    // Go through the source array..
    foreach ($array as $item) {
      // Init or increment the batchSize.
      if (isset($aggregator[$item->{$groupBy}]['batchSize'])) {
        ++$aggregator[$item->{$groupBy}]['batchSize'];
      }
      else {
        $aggregator[$item->{$groupBy}]['batchSize'] = 0;
      }

      if (isset($aggregator[$item->{$groupBy}]['string'])) {
        $aggregator[$item->{$groupBy}]['string'] .= $item->{$stringifyBy} . $separator;
      }
      else {
        $aggregator[$item->{$groupBy}]['string'] = $item->{$stringifyBy} . $separator;
      }

      // When batchLimit is reached or this is the last iteration,
      // add the string to the list, and reset the other values.
      if (($arrayIndex === ($arraySize - 1)) || $aggregator[$item->{$groupBy}]['batchSize'] === ($batchLimit - 1)) {
        $aggregator[$item->{$groupBy}][] = $aggregator[$item->{$groupBy}]['string'];
        $aggregator[$item->{$groupBy}]['string'] = '';
        $aggregator[$item->{$groupBy}]['batchSize'] = 0;
      }

      ++$arrayIndex;
    }

    // Unset stuff that's not needed and remove surplus commas.
    foreach ($aggregator as $accId => $item) {
      foreach ($item as $key => $data) {
        $aggregator[$accId][$key] = trim($aggregator[$accId][$key], $separator);

        if ('batchSize' === $key) {
          unset($aggregator[$accId][$key]);
        }
        elseif ('string' === $key) {
          if (!empty($aggregator[$accId][$key])) {
            $aggregator[$accId][] = $aggregator[$accId][$key];
          }

          unset($aggregator[$accId][$key]);
        }
      }
    }

    return $aggregator;
  }

}
