<?php

namespace Drupal\twitter_ads_api\Api\v1;

use Abraham\TwitterOAuth\TwitterOAuth;

/**
 * Class TwitterAdsApi.
 *
 * @see https://dev.twitter.com/ads/reference when planning to extend this.
 *
 * @package Drupal\twitter_ads_api\Api\v1
 */
class TwitterAdsApi {

  /**
   * The URL for the sandbox twitter ads API.
   *
   * @var string
   */
  const SANDBOX_URL = 'https://ads-api-sandbox.twitter.com';

  /**
   * The URL for the live twitter ads API.
   *
   * @var string
   */
  const LIVE_URL = 'https://ads-api.twitter.com';

  /**
   * The twitter app Consumer/API key.
   *
   * Note: Generate at https://apps.twitter.com/ .
   *
   * @var string|null
   */
  private $consumerKey;

  /**
   * The twitter app Consumer/API secret.
   *
   * Note: Generate at https://apps.twitter.com/ .
   *
   * @var string|null
   */
  private $consumerSecret;

  /**
   * The twitter app access token.
   *
   * Note: Generate at https://apps.twitter.com/ .
   *
   * @var string
   */
  private $oauthToken;

  /**
   * The twitter app access secret.
   *
   * Note: Generate at https://apps.twitter.com/ .
   *
   * @var string
   */
  private $oauthSecret;

  /**
   * The version of the used ads API.
   *
   * @var string
   */
  private $apiVersion = '1';

  /**
   * The API URL.
   *
   * Note: this is only usable with the Brainsum fork of the Twitter OAuth API.
   * The original OAuth doesn't support setting the host.
   *
   * @var string|null
   */
  private $apiUrl;

  /**
   * The OAuth connection object.
   *
   * @var \Abraham\TwitterOAuth\TwitterOAuth|null
   */
  private $apiConnection;

  /**
   * TwitterAdsApi constructor.
   *
   * @param null|string $consumerKey
   *   The twitter app Consumer/API key.
   * @param null|string $consumerSecret
   *   The twitter app Consumer/API secret.
   * @param null|string $oauthToken
   *   The twitter app access token.
   * @param null|string $oauthSecret
   *   The twitter app access secret.
   * @param null|string $apiVersion
   *   The version of the API.
   * @param bool $isSandboxCall
   *   TRUE for calling the ads sandbox API, FALSE for calling the ads live API.
   */
  public function __construct($consumerKey = NULL, $consumerSecret = NULL, $oauthToken = NULL, $oauthSecret = NULL, $apiVersion = NULL, $isSandboxCall = FALSE) {
    $this->consumerKey = $consumerKey;
    $this->consumerSecret = $consumerSecret;
    $this->oauthToken = $oauthToken;
    $this->oauthSecret = $oauthSecret;
    $this->apiVersion = (NULL === $apiVersion) ? $this->apiVersion : $apiVersion;

    $this->apiUrl = (FALSE === $isSandboxCall) ? self::LIVE_URL : self::SANDBOX_URL;

    $this->createOAuth();
  }

  /**
   * Create the OAuth object.
   *
   * The consumer can make requests on the behalf of the access/oauth token.
   */
  private function createOAuth() {
    $this->apiConnection = new TwitterOAuth(
      $this->consumerKey,
      $this->consumerSecret,
      $this->oauthToken,
      $this->oauthSecret,
      $this->apiVersion,
      $this->apiUrl
    );
  }

  /**
   * Return the IDs of the found accounts.
   *
   * @return array|null
   *   The array of IDs or NULL when none are found.
   */
  public function getAccountIds() {
    $response = $this->apiConnection->get('accounts');

    if ($response->total_count <= 0) {
      // @todo: Maybe throw a "not found"/"no content" exception.
      return NULL;
    }

    $accountIds = [];

    foreach ($response->data as $account) {
      $accountIds[] = $account->id;
    }

    return $accountIds;
  }

  /**
   * Return the account object.
   *
   * @param string $accountId
   *   The id of the account.
   *
   * @return null|\stdClass
   *   NULL when not found, the account object in other cases.
   */
  public function getAccountById($accountId) {
    $response = $this->apiConnection->get('accounts', [
      'account_id' => $accountId,
    ]);

    if ($response->total_count <= 0) {
      // @todo: Maybe throw a "not found" exception.
      return NULL;
    }

    return $response->data[0];
  }

  /**
   * Get every available campaign, drafts included.
   *
   * @return \stdClass[]
   *   The array of campaign objects.
   */
  public function getCampaigns() {
    $accountIds = $this->getAccountIds();

    $campaigns = [];

    $baseParameters = [
      // 1000 is the max / request.
      'count' => 1000,
    ];

    // @todo: Support pagination.
    // @see https://dev.twitter.com/ads/basics/pagination
    foreach ($accountIds as $accountId) {
      $callParameters = $baseParameters;

      $response = $this->apiConnection->get(
        "accounts/$accountId/campaigns",
        $callParameters
      );

      foreach ($response->data as $campaign) {
        $campaigns[] = $campaign;
      }

      $callParameters = $baseParameters + ['draft_only' => TRUE];

      $response = $this->apiConnection->get(
        "accounts/$accountId/campaigns",
        $callParameters
      );

      foreach ($response->data as $campaign) {
        $campaigns[] = $campaign;
      }
    }

    return $campaigns;
  }

  /**
   * Get every stat available, grouped by account id.
   *
   * Note: Data will be queried in a one week interval from $startDate.
   *
   * @param int $startingDate
   *   Unix timestamp of the starting date.
   *
   * @return array
   *   Stat objects (stdClass) grouped by account id.
   */
  public function getEveryStats($startingDate) {
    /** @var string[] $accountIds */
    $accountIds = $this->getAccountIds();

    $stats = [];

    /** @var \stdClass[][] $aggregatedIds */
    $aggregatedIds = HelperFunctions::stringifyAndGroupArray($this->getCampaigns(), 'account_id', 'id', ',', 20);

    foreach ($accountIds as $accountId) {
      foreach ($aggregatedIds[$accountId] as $campaignIds) {
        $stats[$accountId][] = $this->getAccountStats($accountId, $campaignIds, $startingDate);
      }
    }

    return $stats;
  }

  /**
   * Get the account stats for campaigns.
   *
   * The time interval is [start_date;start_date+7days].
   *
   * @param string $accountId
   *   The account id.
   * @param string $entityIds
   *   A comma-separated list of max. 20 campaign ids.
   * @param int $startDate
   *   The start date as unix timestamp.
   *
   * @return array
   *   The stats.
   */
  public function getAccountStats($accountId, $entityIds, $startDate) {
    $date = new \DateTime();
    $date->setTimestamp($startDate);
    $date->setTime(0, 0, 0);

    $stats = [];
    $callParameters = [
      'entity' => 'CAMPAIGN',
      'entity_ids' => $entityIds,
      'start_time' => $date->format(\DateTime::ATOM),
      'end_time' => $date->modify('+7 days')->format(\DateTime::ATOM),
      'metric_groups' => 'ENGAGEMENT,WEB_CONVERSION,MOBILE_CONVERSION,MEDIA,VIDEO,BILLING',
      'placement' => 'ALL_ON_TWITTER',
      'granularity' => 'HOUR',
    ];

    if ($startDate >= strtotime('2015-07-14 00:00:00 +0000')) {
      $callParameters['metric_groups'] .= ',LIFE_TIME_VALUE_MOBILE_CONVERSION';
    }

    $response = $this->apiConnection->get(
      "stats/accounts/$accountId",
      $callParameters
    );

    // @todo: I hope this won't fail when only one element is returned.
    // That would also mean an inconsistency in the twitter API.
    // @see: https://dev.twitter.com/ads/reference/1/get/stats/accounts/account_id
    /** @var \stdClass $stat */
    foreach ($response->data as $stat) {
      $stats[] = $stat;
    }

    return $stats;
  }

  /**
   * Get every insight for all accounts.
   *
   * @return array
   *   An array of insight data.
   */
  public function getInsights() {
    /** @var string[] $accountIds */
    $accountIds = $this->getAccountIds();

    $insights = [];

    foreach ($accountIds as $accountId) {
      $insights[] = $this->getInsightsByAccountId($accountId);
    }

    return $insights;
  }

  /**
   * Get every insight for a single account.
   *
   * @param string $accountId
   *   The ID of the account.
   *
   * @return array
   *   An array of insight data.
   */
  public function getInsightsByAccountId($accountId) {
    $callParameters = [
      'audience_type' => 'ALL_ON_TWITTER',
    ];

    $response = $this->apiConnection->get(
      "insights/accounts/$accountId",
      $callParameters
    );

    $data = [];

    foreach ($response->data as $item) {
      $data[] = $item;
    }

    return $data;
  }

}
